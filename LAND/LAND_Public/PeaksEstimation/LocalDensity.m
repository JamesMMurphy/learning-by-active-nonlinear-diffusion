function [Density,PWdist] = LocalDensity(X,K,PWdist)

% Computes the local densitys of the points in an N x D matrix X, using the
% threshold value th.  Use K nearest neighbor.  A matrix of distances may
% be passed in.  

Density=zeros(size(X,1),1);

if nargin<3
    D=pdist(X);
    PWdist=squareform(D); %Pairwise distances
end

%For all real HSI experiments

sigma=.05*mean(PWdist(PWdist>0));

PWdist=sort(PWdist);

for l=1:size(PWdist,1)
    Density(l)=sum(exp(-(PWdist(1:K,l).^2)/(sigma^2)));
end
    
    
end