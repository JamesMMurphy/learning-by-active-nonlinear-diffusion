function A = MakeAdjacency(X,k)

% Function that computes the adjacency matrix of the graph formed by
% connecting each point of X with its k nearest neighbors (enusres it is
% symmetric)

n=size(X,1);

[IDX,D] = knnsearch(X,X,'K',k);
Base=ones(n,k);

for i=1:n
    Base(i,:)=i*Base(i,:);
end

D=D';
IDX=IDX';
Base=Base';

A=sparse(Base(:),IDX(:),D(:),n,n);

A=max(A,A'); 


end

        