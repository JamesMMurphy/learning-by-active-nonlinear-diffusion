function [Centers, G, DistStruct]=DensityPeaksEstimation(X, K, DensityNN, PeakOpts)

% DeltaDensity is the product of a kernel density estimator and a measure
% of distance from the point to a point of higher kernel density.  Points
% with large values should have both high densities and be far from points
% with higher densities.

PWdist=squareform(pdist(X)); %Pairwise distances

[DeltaDensity, Density, Delta, G, D]=FindDensityPeaks(X,DensityNN,PWdist,PeakOpts);

DistStruct.DeltaDensity=DeltaDensity;
DistStruct.Density=Density;
DistStruct.Delta=Delta;
DistStruct.D=D;

% Use the largest delta density values to learn cores.

[~,idx]=sort(DeltaDensity,'descend');

Centers=idx(1:K)'; 

end
