function Labels = LAND(X,K,DistStruct,Centers,GT,Queries)

% Run the LAND algorithm

%% Initialize variables

GT=UniqueGT(GT);
Labels=zeros(1,size(X,1));
Density=DistStruct.Density;
D=DistStruct.D;

%% If no budget, run the unsupervised algorithm.

if isempty(Queries)
    
    for k=1:K
        Labels(Centers(k))=k;
    end
    
end

%% If budget, run LAND

if ~isempty(Queries)
    for j=1:length(Queries)
        Labels(Queries(j))=GT(Queries(j));
    end 
end

%% Sort points by density

[~,idx]=sort(Density,'descend');

%% Label highest density point if necessary 

if Labels(idx(1))==0
    Labels(idx(1))=GT(idx(1));
end

%% First pass

for j=1:length(Labels)
    if Labels(idx(j))==0
        try
            [~,NN]=min(D(idx(j),idx(1:j-1)));
            Labels(idx(j))=Labels(idx(NN));
        catch
            keyboard
        end
        % Special tie-breaking case when many value have same density
        if Labels(idx(j))==0
            temp=find(Labels>0);
            [~,NN]=min(D(idx(j),temp));
            Labels(idx(j))=Labels(temp(NN));
        end
    end
end


end

