function [DeltaDensity, Density, Delta, G, PWdist] = FindDensityPeaks(X,K,D,PeakOpts)

% Compute the density peaks of the dataset X.
% Compute the true finite-sample Density and the distances Delta from each point to the
% point of higher density.

%% Compute density and initialize Delta

if nargin<3
    [Density,PWdist]=LocalDensity(X,K);
else 
   [Density,PWdist]=LocalDensity(X,K,D);
end

Delta=zeros(size(Density));

%% Compute pairwise distances to be used for Delta computation

if strcmp(PeakOpts.ModeDetection, 'Euclidean')
    PWdist=squareform(pdist(X));
    G=[];
    
elseif strcmp(PeakOpts.ModeDetection,'LLPD')
    LLPDopts.ThresholdMethod='Percentiles';
    LLPDopts.PercentileSize=2;
    PWdist=FasterApproxLLPD(X,20,size(X,1),LLPDopts);
    
elseif  strcmp(PeakOpts.ModeDetection, 'Diffusion')
    [PWdist,G]=DiffusionDistance(X,PeakOpts.DiffusionTime,PeakOpts.DiffusionOpts);
    
end

%% Compute Delta by measuring the minimal distance to a point of higher density

for i=1:length(Density)
    if ~(Density(i)==max(Density))
        Delta(i)=min(PWdist(Density>Density(i),i));     
    else
        Delta(i)=max(PWdist(i,:)); 
    end
end

%% Normalize 

Delta=Delta/max(Delta);
Density=Density/sum(Density);

%% Combine these measures multiplicatively, as suggested in FSFDPC paper (Science, 2014)

DeltaDensity=Delta.*(Density);

end