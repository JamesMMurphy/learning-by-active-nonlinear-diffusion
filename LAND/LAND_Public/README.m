% Implementation of the Learning by Active Nonlinear Diffusion (LAND)
% algorithm by Maggioni and Murphy.  The script PaperExperimentsScript.m
% runs the experiments in the paper:
%
% Maggioni, Mauro, and James M. Murphy, "Learning by Active Nonlinear 
% Diffusion." arXiv preprint arXiv:1905.12989 (2019).
%
% If this code is used in any publications, please cite the above paper.
%
% The following packages are necessary to run LAND and compare against ground truth:
%
% Diffusion Geometry: https://mauromaggioni.duckdns.org/#tab_DiffusionGeom
% Hungarian Algorithm: https://www.mathworks.com/matlabcentral/fileexchange/20328-munkres-assignment-algorithm
%
% Copyright James M. Murphy, 2019.  

