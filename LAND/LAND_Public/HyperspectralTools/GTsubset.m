function GTnew= GTsubset(GTold,N,classes)
% Extracts a random subset of the ground truth, so that each class has N representatives.

if size(GTold,2)>1 % HSI data
    for k=1:classes
        idxs{k}=find(GTold(k,:)==1);
        idxsuse{k}=datasample(idxs{k},min(N,length(idxs{k})),'Replace',false);
    end
end
if size(GTold,2)==1
    for k=1:classes
        idxs{k}=find(GTold==k);
        idxsuse{k}=datasample(idxs{k},N,'Replace',false);
    end
end

for k=1:classes
    try
        GTnew(:,k)=idxsuse{k};
    catch
        keyboard
    end
end

end
