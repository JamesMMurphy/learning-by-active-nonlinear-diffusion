function [LeafPurity,TotalPurity] = OverallPurity(LearnedLabels,GT_Labels,K)

for k=1:K
    LeafPurity(k)=Purity(GT_Labels(find(LearnedLabels==k)));
    LeafCounts(k)=sum(LearnedLabels==k);
end

TotalPurity=sum(LeafPurity.*LeafCounts)/length(LearnedLabels);

end
