function [X,LabelsGT,K_GT] = ExperimentalData(DataSet)

if strcmp(DataSet,'SyntheticSpherical')
    
    X1=mvnrnd([0,1],.1*[1,0;0,1],300);
    Labels1(1:300)=1;
    X2=mvnrnd([1,0],.1*[1,0;0,1],300);
    Labels2(1:300)=2;
    X3=mvnrnd([.5,-.9],.01*[1,0;0,1],50);
    Labels3(1:50)=3;
    
    X=vertcat(X1,X2,X3);
    LabelsGT=horzcat(Labels1,Labels2,Labels3);
    K_GT=3;
    
    
elseif strcmp(DataSet,'SyntheticGeometric')
    
    X1=2*rand(4000,2)-[1,1];
    X1=.5*X1(logical((sum(X1.^2,2)<=1).*(sum(X1.^2,2)>=.5)),:);
    Labels1(1:size(X1,1))=1;
    X2=vertcat(rand(800,2),[1,.1].*rand(100,2)+[0,.9],[1,.1].*rand(100,2));
    X2=[.1,2].*X2+[.75,-1];
    Labels2(1:size(X2,1))=2;
    X3=mvnrnd([0,0],.005*[1,0;0,1],150);
    Labels3(1:size(X3,1))=3;
    
    X=vertcat(X1,X2,X3);
    LabelsGT=horzcat(Labels1,Labels2,Labels3);
    K_GT=3;
    
    
elseif strcmp(DataSet,'SyntheticBottleneck')
    
    X1=2*rand(500,2)-[1,1];
    X1=X1(sum(X1.^2,2)<1,:);
    Labels1(1:size(X1,1))=1;
    
    X2=2*rand(500,2)-[1,1];
    X2=X2(sum(X2.^2,2)<1,:)+[4,0];
    Labels2(1:size(X2,1))=2;
    
    X3=2*rand(500,2)-[1,1];
    X3=X3(sum(X3.^2,2)<1,:)+[2,2];
    Labels3(1:size(X3,1))=3;
    
    X4=2*rand(500,2)-[1,1];
    X4=X4(sum(X4.^2,2)<1,:)+[2,-2];
    Labels4(1:size(X4,1))=4;
    
    
    Bridge1=zeros(80,2);
    Bridge1(:,1)=2*rand(80,1)+1;
    LabelsBridge1=zeros(1,80);
    LabelsBridge1(Bridge1(:,1)<2)=1;
    LabelsBridge1(Bridge1(:,1)>=2)=2;
    
    Bridge2=2*ones(80,2);
    Bridge2(:,2)=2*rand(80,1)-1;
    LabelsBridge2=zeros(1,80);
    LabelsBridge2(Bridge2(:,2)>0)=3;
    LabelsBridge2(Bridge2(:,2)<=0)=4;
    
    X=vertcat(X1,X2,X3,X4,Bridge1,Bridge2);
    LabelsGT=horzcat(Labels1,Labels2,Labels3,Labels4,LabelsBridge1,LabelsBridge2);
    K_GT=4;
    
elseif strcmp(DataSet,'SalinasA')
    
    DataInput.datasource='HSI';
    DataInput.HSIname='SalinasA';
    DataInput.idx1=1:83;
    DataInput.idx2=1:86;
    
    [~,~,~,~,K_GT,DataParameters]=MakeHSI(DataInput.HSIname,DataInput.idx1,DataInput.idx2);
    LabelsGT=reshape(DataParameters.GT,1,length(DataInput.idx1)*length(DataInput.idx2));
    
    % Run on all pixels
    
    X=DataParameters.RawHSI;
    X=reshape(X,size(X,1)*size(X,2),size(X,3));
    X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1); %Normalize X
    
elseif strcmp(DataSet,'Pavia')
    
    DataInput.datasource='HSI';
    DataInput.HSIname='Pavia';
    DataInput.idx1=101:400;
    DataInput.idx2=241:300;
    
    [~,~,~,~,K_GT,DataParameters]=MakeHSI(DataInput.HSIname,DataInput.idx1,DataInput.idx2);
    LabelsGT=reshape(DataParameters.GT,1,length(DataInput.idx1)*length(DataInput.idx2));
    
    % Run on all pixels
    
    X=DataParameters.RawHSI;
    X=reshape(X,size(X,1)*size(X,2),size(X,3));
    X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1); %Normalize X
   

LabelsGT=UniqueGT(LabelsGT);

end
