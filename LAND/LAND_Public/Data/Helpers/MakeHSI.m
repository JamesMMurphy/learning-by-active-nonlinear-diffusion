% Function to generate desired HSI data and put it in the correct format.

function [X,Y,Ytuple,d,K_GT,DataParameters] = MakeHSI(HSIname,idx1,idx2 )

% HSIname =  Name is HSI dataset to generate data from.
% idx1 = Rows of HSI image to use
% idx2 = Columns of HSI image to use

% [M,N] = spatial dimensions of HSI data generated, before concatentation


%%
if strcmp(HSIname,'Pavia')
    
    load('Pavia.mat');
    load('Pavia_gt.mat');
    
    X=pavia(idx1,idx2,:);
    X=permute(X,[2,1,3]);
    Y=double(pavia_gt(idx1,idx2));
    Y=Y';
end

%%

if strcmp(HSIname,'SalinasA')
    
    load('SalinasA_smallNoise.mat');
    load('SalinasA_gt.mat');
    
    Y=double(salinasA_gt(idx1,idx2));
    X=permute(X,[2,1,3]);
    Y=Y;   
    
end


%%  Apply certain treatments to both labelled and unlabelled HSI


%% Only work with ground truth if there is some.

if ~isempty(Y)
    DataParameters.RawHSI=X;
    [DataParameters.M, DataParameters.N,~]=size(DataParameters.RawHSI);
    
    GT=Y;
    Y=reshape(Y,size(Y,1)*size(Y,2),size(Y,3));
    Y=Y';
    
    Y=Y(Y>0);
    Y=UniqueGT(Y);
    
    K_GT=max(Y);
    
    Ytuple=zeros(max(Y),size(Y,2));
    
    for k=1:size(Y,2)
        Ytuple(Y(k),k)=1;
    end
    
    DataParameters.GT=GT;
    
    %%
    
    X=DataParameters.RawHSI;
    X=reshape(X,size(X,1)*size(X,2),size(X,3));
    X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1); %Normalize X
    
    %%
    Y=reshape(DataParameters.GT,1,size(GT,1)*size(GT,2));
    
    d=size(X,2);
    
end

end

