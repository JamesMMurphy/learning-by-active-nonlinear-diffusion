%Given a set of labels, how close to being pure is it?

function PurityScore = Purity(Labels)

    PurityScore=sum(Labels==mode(Labels))/length(Labels);
    
end

