%% Show hierarchical clusters under different criterion: LUND, SLC, ALC

clear all;

DataSet='SyntheticBottleneck';

[X,LabelsGT,K_GT]=ExperimentalData(DataSet);


%% Bottleneck

if strcmp(DataSet,'SyntheticBottleneck')
    
    PeakOpts.DiffusionOpts.epsilon=.5;
    PeakOpts.DiffusionTime=100;
    
elseif strcmp(DataSet,'SyntheticSpherical')
        
    PeakOpts.DiffusionOpts.epsilon=.5;
    PeakOpts.DiffusionTime=100;
    
elseif strcmp(DataSet,'SyntheticGeometric')
    
    PeakOpts.DiffusionOpts.epsilon=.15;
    PeakOpts.DiffusionTime=10^5;
    
end

%% Set LUND parameters

DensityNN=100;
PeakOpts.DiffusionOpts.K=10;
PeakOpts.DiffusionOpts.kNN=100;
PeakOpts.DiffusionOpts.LazyWalk=0;
M=1;
N=1;
PeakOpts.ModeDetection='Diffusion';
PeakOpts.UserPlot=0;

%% Build clusterings

if strcmp(DataSet,'SyntheticGeometric')
    K_Max=10;
elseif strcmp(DataSet,'SyntheticSpherical')
    K_Max=50;
elseif strcmp(DataSet,'SyntheticBottleneck')
    K_Max=250; 
end

for K=1:K_Max
    
    Tree_SLC = linkage(X,'single');
    Labels_SLC=cluster(Tree_SLC,'maxclust',K);
    Tree_ALC = linkage(X,'average');
    Labels_ALC=cluster(Tree_ALC,'maxclust',K);
    
    [CentersDiffusion, G, DistStruct]=DensityPeaksEstimation(X, K, DensityNN, PeakOpts);
    
    Labels_LUND=FSFclustering(X,K,DistStruct,CentersDiffusion);
    
    
    %% Compute purity across scales
    
    [~,OverallPurity_ALC(K)]=OverallPurity(Labels_ALC,LabelsGT,K);
    [~,OverallPurity_SLC(K)]=OverallPurity(Labels_SLC,LabelsGT,K);
    [~,OverallPurity_LUND(K)]=OverallPurity(Labels_LUND,LabelsGT,K);
    
end

%% Plot purity at different scales

figure;
scatter(X(:,1),X(:,2),[],LabelsGT);
title('Labeled Data','Interpreter','latex','FontSize',18);
axis square;

figure;
plot(OverallPurity_ALC,'LineWidth',3);
hold on;
plot(OverallPurity_SLC,'LineWidth',3);
hold on;
plot(OverallPurity_LUND,'LineWidth',3);
legend('ALC','SLC','LUND','Location','southeast');
title('Purity','FontSize',14,'Interpreter','latex');
xlabel('Number of Leaves','FontSize',14,'Interpreter','latex');
axis square