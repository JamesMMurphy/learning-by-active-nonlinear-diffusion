% Show how multiscale structure in data is revealed through LUND/LAND via
% the time parameter t.

clear all;
close all; 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Spherical data %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%


X1=mvnrnd([0,1],.1*[1,0;0,1],300);
X2=mvnrnd([1,0],.1*[1,0;0,1],300);
X3=mvnrnd([.5,-.9],.01*[1,0;0,1],50);
X=vertcat(X1,X2,X3);
Labels=vertcat(ones(size(X1,1),1),2*ones(size(X2,1),1),3*ones(size(X3,1),1));

figure;
scatter(X(:,1),X(:,2),[],Labels);
axis square;

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% NonSpherical data %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

X1=2*rand(4000,2)-[1,1];
X1=.5*X1(logical((sum(X1.^2,2)<=1).*(sum(X1.^2,2)>=.5)),:);
X2=vertcat(rand(800,2),[1,.1].*rand(100,2)+[0,.9],[1,.1].*rand(100,2));
X2=[.1,2].*X2+[.75,-1];
X3=mvnrnd([0,0],.005*[1,0;0,1],150);
X=vertcat(X1,X2,X3);
Labels=vertcat(ones(size(X1,1),1),2*ones(size(X2,1),1),3*ones(size(X3,1),1));

figure;
scatter(X(:,1),X(:,2),[],Labels);
axis square;
axis([ -.75 1.25 -1 1])

%% Run LAND, small time

DensityNN=100;
PeakOpts.DiffusionOpts.K=10;
PeakOpts.DiffusionOpts.kNN=size(X,1);
PeakOpts.DiffusionOpts.LazyWalk=0;
PeakOpts.DiffusionOpts.epsilon=.15;
PeakOpts.DiffusionTime=100;
M=1;
N=1;
PeakOpts.ModeDetection='Diffusion';
PeakOpts.UserPlot=0;
    
[CentersDiffusion, ~ ,DistStruct]= DensityPeaksEstimation(X, 3, DensityNN, PeakOpts);

h=figure;
scatter(X(:,1),X(:,2),[],log10(DistStruct.DeltaDensity));
hold on;
scatter(X(CentersDiffusion,1),X(CentersDiffusion,2),[],'r','filled');
colorbar
axis square;
axis([ -.75 1.25 -1 1])
title('Small Time','Interpreter','latex','FontSize',14)

figure;
imagesc(DistStruct.D);
colorbar
axis square;
title('Small Time','Interpreter','latex','FontSize',14)

%% Run LAND, large time

PeakOpts.DiffusionTime=10^5;

[CentersDiffusion, G, DistStruct]=...
    DensityPeaksEstimation(X, 3, DensityNN, PeakOpts);

figure;
scatter(X(:,1),X(:,2),[],log10(DistStruct.DeltaDensity));
hold on;
scatter(X(CentersDiffusion,1),X(CentersDiffusion,2),[],'r','filled');
colorbar
axis square;
axis([ -.75 1.25 -1 1])
title('Large Time','Interpreter','latex','FontSize',14)

figure;
imagesc(DistStruct.D);
colorbar
axis square;
title('Large Time','Interpreter','latex','FontSize',14)