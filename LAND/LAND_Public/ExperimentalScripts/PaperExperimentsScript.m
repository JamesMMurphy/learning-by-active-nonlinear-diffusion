%% Compare LAND, and DH for different datasets and budgets.  
% All datasets are taken from the paper :
% Maggioni, M. and J.M. Murphy
% "Learning by Active Nonlinear Diffusion." 
% arXiv preprint arXiv:1905.12989 (2019).

clear all;
close all;

%% Select dataset

%{
DataSet='SyntheticBottleneck';
M=0;
N=0;
Budget=1:1:170;
PeakOpts.DiffusionOpts.epsilon=.5;
PeakOpts.DiffusionOpts.K=10;
PeakOpts.DiffusionTime=100;
%}

DataSet='SyntheticSpherical';
M=0;
N=0;
Budget=[1,5:5:65];
PeakOpts.DiffusionOpts.epsilon=.5;
PeakOpts.DiffusionOpts.K=10;
PeakOpts.DiffusionTime=100;

%{
DataSet='SyntheticGeometric';
M=0;
N=0;
Budget=1:1:20;
PeakOpts.DiffusionOpts.epsilon=.15;
PeakOpts.DiffusionOpts.K=10;
PeakOpts.DiffusionTime=10^4.5;
%}

%{
DataSet='Pavia';
M=60;
N=300;
Budget=10:10:500;
PeakOpts.DiffusionOpts.epsilon=1;
PeakOpts.DiffusionOpts.K='automatic';
PeakOpts.DiffusionTime=30;
%}

%{
DataSet='SalinasA';
M=83;
N=86;
Budget=10:10:2000;
PeakOpts.DiffusionOpts.epsilon=1;
PeakOpts.DiffusionOpts.K='automatic';
PeakOpts.DiffusionTime=30;
%}

%% Set parameters

[X,LabelsGT,K_GT]=ExperimentalData(DataSet);

PeakOpts.UserPlot=0;

%How many nearest neighbors to use for diffusion distance
PeakOpts.DiffusionOpts.kNN=100; %For comparison with SpatialDiffusion%100;

%Force probability of self-loop to exceed .5.
PeakOpts.DiffusionOpts.LazyWalk=0;

%How many nearest neighbors to use for KDE
DensityNN=20;

%Mode detection
PeakOpts.ModeDetection='Diffusion';

%% Find densities, diffusion distances, and data modes

[Centers, G, DistStruct] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts);

%%  Compute active learning queries

CandidateQueries=find(LabelsGT>0);
[~,Idx]=sort(DistStruct.DeltaDensity(CandidateQueries),'descend');
CandidateQueries=CandidateQueries(Idx);

Queries_LAND=CandidateQueries;
Queries_Random=CandidateQueries(randperm(length(CandidateQueries)));

%%  Compute LAND labels

for k=1:length(Budget)
    LabelsLAND=LAND(X,K_GT,DistStruct,Centers,LabelsGT,Queries_LAND(1:Budget(k)));
    LabelsRandom=LAND(X,K_GT,DistStruct,Centers,LabelsGT,Queries_Random(1:Budget(k)));
    [OA_LAND(k), AA_LAND(k), kappa_LAND(k)] = GetAccuracies(LabelsLAND(LabelsGT>0),UniqueGT(LabelsGT(LabelsGT>0)),K_GT);
    [OA_Random(k), AA_Random(k), kappa_Random(k)] = GetAccuracies(LabelsRandom(LabelsGT>0),UniqueGT(LabelsGT(LabelsGT>0)),K_GT);
end

%%  Plot HSI data, GT, Results

if M>0 && N>0
    
    figure;
    imagesc(reshape(sum(X,2),M,N));
    axis equal
    axis off;
    axis tight;
    colormap('gray');
    
    figure;
    imagesc(reshape(LabelsGT,M,N));
    axis equal
    axis off;
    axis tight;
    
end

figure;
plot(OA_LAND,'-','LineWidth',3,'Color','r');
hold on;
plot(OA_Random,'--','LineWidth',3,'Color','r');
hold on;
plot(AA_LAND,'-','LineWidth',3,'Color','b');
hold on;
plot(AA_Random,'--','LineWidth',3,'Color','g');
hold on;
plot(kappa_LAND,'-','LineWidth',3,'Color','g');
hold on;
plot(kappa_Random,'--','LineWidth',3,'Color','b');

lgd=legend('OA, LAND','OA, Random','AA, LAND','AA, Random','$\kappa$, LAND','$\kappa$, Random',...
        'Interpreter','latex','Location','southeast');
lgd.NumColumns = 3;
axis([1 length(Budget) min(vertcat(OA_LAND(:),AA_LAND(:),kappa_LAND(:),OA_Random(:),AA_Random(:),kappa_Random(:)))-.05 1])
xticks([1,2:1:length(Budget)])
xticklabels(Budget([1,2:1:length(Budget)]))
title('LAND Accuracy','Interpreter','latex','FontSize',14)
xlabel('Number Queries','Interpreter','latex','FontSize',14);
ylabel('Accuracy','Interpreter','latex','FontSize',14);

