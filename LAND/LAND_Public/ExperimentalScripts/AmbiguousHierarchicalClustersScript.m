% Show that LUND captures ambiguous cluster structure through the diffusion
% time scale t.

clear all;
close all; 

%% Generate point cloud data with multiscale structure

X1=[.2,.2].*randn(100,2);
X2=[.2,.2].*randn(100,2)+[1.5,0];
X3=[.2,.2].*randn(100,2)+[1.5,2];
X4=[.2,.2].*randn(100,2)+[0,2];
X=vertcat(X1,X2,X3,X4);

Labels_TwoClusters(1:200)=1;
Labels_TwoClusters(201:400)=2;

Labels_FourClusters(1:100)=1;
Labels_FourClusters(101:200)=2;
Labels_FourClusters(201:300)=3;
Labels_FourClusters(301:400)=4;

figure;
scatter(X(:,1),X(:,2));
axis([min(X(:,1))-.25, max(X(:,1))+.25, min(X(:,2))-.25, max(X(:,2))+.25]);
title('Data To Cluster','FontSize',14,'Interpreter','latex');
axis equal

figure;
scatter(X(:,1),X(:,2),[],Labels_TwoClusters);
axis([min(X(:,1))-.25, max(X(:,1))+.25, min(X(:,2))-.25, max(X(:,2))+.25]);
title('Data Clustered Into Two Clusters','FontSize',14,'Interpreter','latex');
axis equal

figure;
scatter(X(:,1),X(:,2),[],Labels_FourClusters);
axis([min(X(:,1))-.25, max(X(:,1))+.25, min(X(:,2))-.25, max(X(:,2))+.25]);
axis equal
title('Data Clustered Into Four Clusters','FontSize',14,'Interpreter','latex');


%% Set LUND parameters

K=10; %Just to get some Dt values
PeakOpts.DiffusionOpts.epsilon=.5;

DensityNN=100;
PeakOpts.DiffusionOpts.K=10;
PeakOpts.DiffusionOpts.kNN=100;
PeakOpts.DiffusionOpts.LazyWalk=0;
M=1;
N=1;
PeakOpts.ModeDetection='Diffusion';
PeakOpts.UserPlot=0;
Times=10.^[.6:.2:6];

%%  Show the behavior of the mode estimation procedure as a function of t

for k=1:length(Times)
    
    PeakOpts.DiffusionTime=Times(k);
    
    [CentersDiffusion, ~, DistStruct]=...
        DensityPeaksEstimation(X, K, DensityNN, PeakOpts);
    
    SortedDeltaDensity=sort(DistStruct.DeltaDensity,'descend');
    
    [~,K_hat(k)]=max(SortedDeltaDensity(1:end-1)./SortedDeltaDensity(2:end));
    
end

figure;
plot(K_hat,'LineWidth',3);
title('Estimate on $K$ as a function of $t$','Interpreter','latex','FontSize',14);
axis([-Inf Inf 1 6]);
xticks(1:3:28);
xticklabels(log10(Times(1:3:end)));
xlabel('$\log_{10}(t)$','Interpreter','latex','FontSize',14);
ylabel('$\hat{K}$','Interpreter','latex','FontSize',14);
axis square;

%% LAND for small time

PeakOpts.DiffusionTime=10^1.5;

[~, ~, DistStruct]=DensityPeaksEstimation(X, K, DensityNN, PeakOpts);
[~,Queries_Active]=sort(DistStruct.DeltaDensity,'descend');

LabelsLAND_SmallTime_FourClasses=LAND(X,4,DistStruct,CentersDiffusion,Labels_FourClusters,Queries_Active(1:4));
LabelsLAND_SmallTime_TwoClasses=LAND(X,2,DistStruct,CentersDiffusion,Labels_TwoClusters,Queries_Active(1:4));

figure;
imagesc(DistStruct.D)
axis square;
colorbar

figure;
scatter(X(:,1),X(:,2),[],LabelsLAND_SmallTime_FourClasses);
axis equal;
title('LAND Labels, Small Time, Four Classes', 'FontSize', 14, 'Interpreter', 'latex');

figure;
scatter(X(:,1),X(:,2),[],LabelsLAND_SmallTime_TwoClasses);
axis equal;
title('LAND Labels, Small Time, Two Classes', 'FontSize', 14, 'Interpreter', 'latex');

%% LAND for large time

PeakOpts.DiffusionTime=10^5;

[Centers, G, DistStruct]=...
    DensityPeaksEstimation(X, K, DensityNN, PeakOpts);

LabelsLAND_LargeTime_FourClasses=LAND(X,4,DistStruct,CentersDiffusion,Labels_FourClusters,Queries_Active(1:4));
LabelsLAND_LargeTime_TwoClasses=LAND(X,2,DistStruct,CentersDiffusion,Labels_TwoClusters,Queries_Active(1:4));

figure;
imagesc(DistStruct.D)
axis square;
colorbar

figure;
scatter(X(:,1),X(:,2),[],LabelsLAND_LargeTime_FourClasses);
axis equal;
title('LAND Labels, Large Time, Four Classes', 'FontSize', 14, 'Interpreter', 'latex');

figure;
scatter(X(:,1),X(:,2),[],LabelsLAND_LargeTime_TwoClasses);
axis equal;
title('LAND Labels, Large Time, Two Classes', 'FontSize', 14, 'Interpreter', 'latex');