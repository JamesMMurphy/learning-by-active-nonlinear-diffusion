% Implementation of the Spatially Regularized Learning by Active Nonlinear 
% Diffusion (SR LAND) algorithm by J.M. Murphy.  The script PaperExperimentsScript.m
% runs the experiments in the paper:
%
% J.M. Murphy. "Spatially regularized active diffusion learning 
% for high-dimensional images." Pattern Recognition Letters.  2020.
%
% If this code is used in any publications, please cite the above paper.
%
% The following packages are necessary to run SR LAND and compare against ground truth:
%
% Diffusion Geometry: https://mauromaggioni.duckdns.org/#tab_DiffusionGeom
% Hungarian Algorithm: https://www.mathworks.com/matlabcentral/fileexchange/20328-munkres-assignment-algorithm
%
% The necessary publically available HSI data may be found here: 
%
% http://www.ehu.eus/ccwintco/index.php/Hyperspectral_Remote_Sensing_Scenes
%
% The following packages are necessary to reproduce the results in the
% above paper:
%
% -MATLAB Active Learning Toolbox for Remote Sensing: https://github.com/IPL-UV/altoolbox
% -Belief Propagation: https://www.lx.it.pt/~jun/demos.html
% -Image Fusion and Recursive Filtering: http://xudongkang.weebly.com/
%
% To see SR_LAND run, use DemoScript.m
% To see all comparisons in the paper, use ComparisonScript.m
%
%
% Copyright James M. Murphy, 2020.  

