function [Centers, G, DistStruct]=DensityPeaksEstimation(X, K, DensityNN, PeakOpts,M,N)

% DeltaDensity is the product of a kernel density estimator and a measure
% of distance from the point to a point of higher kernel density.  Points
% with large values should have both high densities and be far from points
% with higher densities.


[DeltaDensity, Density, Delta, G, Idx_NN, D_NN]=FindDensityPeaks(X,DensityNN,PeakOpts,M,N);

DistStruct.DeltaDensity=DeltaDensity;
DistStruct.Density=Density;
DistStruct.Delta=Delta;
DistStruct.Idx_NN=Idx_NN;
DistStruct.D_NN=D_NN;

% Use the largest delta density values to learn cores.

[~,SortedDD_Idx]=sort(DeltaDensity,'descend');

Centers(1)=SortedDD_Idx(1);
Needed=K-1;
start=2;

while Needed>0 && start<=size(X,1)
    Centers(end+1)=SortedDD_Idx(start);
    Needed=Needed-1;
    start=start+1;
end

end
