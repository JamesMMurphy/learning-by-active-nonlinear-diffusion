function NN =  FindNearestLabeledNeighbor(X,j,idx,G,G_iterated,PeakOpts)

% Do something different depending on what distance, 
% Euclidean or Diffusion, we want to use.

t=PeakOpts.DiffusionTime;

if strcmp(PeakOpts.ModeDetection,'Euclidean')
    NN_List=G.DistInfo.idxs{idx(j)};
elseif strcmp(PeakOpts.ModeDetection,'Diffusion')
    NN_List=G_iterated.DistInfo.idxs{idx(j)};
end

Use=intersect(NN_List,idx(1:j-1),'stable');

if ~isempty(Use)
    NN=Use(1);
else
    if strcmp(PeakOpts.ModeDetection,'Euclidean')
        PWdist=pdist2(X(idx(j),:),X(idx(1:j-1),:));
    elseif strcmp(PeakOpts.ModeDetection,'Diffusion')
        WeightedEigVecs=(G.EigenVals.^t)'.*G.EigenVecs;
        PWdist=pdist2(WeightedEigVecs(idx(j),:),WeightedEigVecs(idx(1:j-1),:));
    end
    [~,NN]=min(PWdist);
    NN=idx(NN);
end

end