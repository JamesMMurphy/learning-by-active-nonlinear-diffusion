function [DeltaDensity, Density, Delta, G, Idx_NN, D_NN] = FindDensityPeaks(X,K,PeakOpts,M,N)

%Compute the density peaks of the dataset X.
% Compute the true finite-sample Density and the distances Delta from each point to the
% point of higher density.

%% Compute density and initialize Delta

Density=LocalDensity(X,K);
Delta=zeros(size(Density));

%% Compute pairwise distances to be used for Delta computation

if strcmp(PeakOpts.ModeDetection, 'Euclidean')
    [Idx_NN,D_NN]=knnsearch(X,X,'K',100);
    G=[];
    
elseif  strcmp(PeakOpts.ModeDetection, 'Diffusion')
    [Idx_NN,D_NN,G]=DiffusionDistance(X,PeakOpts.DiffusionTime,PeakOpts.DiffusionOpts);
    
elseif strcmp(PeakOpts.ModeDetection, 'SpatialDiffusion')
    
    X_spatial=reshape(X,M,N,size(X,2));
    
    NN_Idx=cell(M,N);
    NN_Counts=zeros(M,N);
    NN_Dists=cell(M,N);
    
    for i=1:M
        for j=1:N
            NN_Idx{i,j}=FindNeighbors([i,j], PeakOpts.Window_Diffusion, M, N);
            NN_Counts(i,j)=length(NN_Idx{i,j});
            DistTemp=...
            sqrt(sum((repmat(squeeze(X_spatial(i,j,:))',size(NN_Idx{i,j},2),1)-X(sub2ind([M,N], NN_Idx{i,j}(1,1:size(NN_Idx{i,j},2)), NN_Idx{i,j}(2,1:size(NN_Idx{i,j},2))),:)).^2,2))';
            [~,Idx]=sort(DistTemp,'ascend');
            NN_Idx{i,j}=NN_Idx{i,j}(:,Idx);
            NN_Idx{i,j}=sub2ind([M,N],NN_Idx{i,j}(1,:),NN_Idx{i,j}(2,:));% Convert back to subscript
            NN_Dists{i,j}=DistTemp(Idx);
        end
    end
        
    PeakOpts.DiffusionOpts.NN_Idx=reshape(NN_Idx,M*N,1);
    PeakOpts.DiffusionOpts.NN_Counts=reshape(NN_Counts,M*N,1);
    PeakOpts.DiffusionOpts.NN_Dists=reshape(NN_Dists,M*N,1);
    PeakOpts.DiffusionOpts.epsilon=mean(cat(2,NN_Dists{:}));

    [Idx_NN,D_NN,G]=DiffusionDistance(X,PeakOpts.DiffusionTime,PeakOpts.DiffusionOpts);
    
    
end

%% Compute Delta by measuring the minimal distance to a point of higher density

try
    for i=1:length(Density)
        if ~(Density(i)==max(Density))
            CandidateDeltas=find(Density(Idx_NN(i,:))>Density(i));
            if ~isempty(CandidateDeltas)
                Delta(i)=min(D_NN(i,CandidateDeltas));
            elseif isempty(CandidateDeltas) %Need to do a full NNsearch
                D_NN_Extended{i}=pdist2(G.WeightedEigVecs,G.WeightedEigVecs(i,:));
                CandidateDeltas=find(Density>Density(i));
                Delta(i)=min(D_NN_Extended{i}(CandidateDeltas));
            end
        else
            Delta(i)=Inf;
        end
    end
catch
    keyboard
end

Delta(Delta==Inf)=max(max(D_NN(:)),max(vertcat(D_NN_Extended{:})));
%% Normalize 

Delta=Delta/max(Delta);
Density=Density/sum(Density);

%% Combine these measures multiplicatively, as suggested in FSFDPC paper (Science, 2014)

DeltaDensity=Delta.*(Density);

end