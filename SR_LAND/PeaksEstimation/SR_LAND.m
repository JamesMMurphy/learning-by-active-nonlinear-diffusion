function Labels = SR_LAND(X,K,DistStruct,I,J,M,N,WindowSize,NumSamples,GT,SampleMethod,G)


%% Initialize variables

GT=UniqueGT(GT);
Labels=zeros(1,size(X,1));
Density=DistStruct.Density;

WeightedEigVecs=G.WeightedEigVecs;
BudgetRemaining=NumSamples; % Number of pixels needed for active learning; initial value is a user input

%% Perform different flavors of active learning

if strcmp(SampleMethod,'Core')
    
    % Query the modes
    [~,SortedDD_Idx]=sort(DistStruct.DeltaDensity,'descend');
    
    j=1;
    
    while BudgetRemaining>0 && j<=length(SortedDD_Idx)
        if  GT(SortedDD_Idx(j))~=0
            Labels(SortedDD_Idx(j))=GT(SortedDD_Idx(j));
            BudgetRemaining=BudgetRemaining-1;
        end
        j=j+1;
        
        % Check to see if we need to stop and use the rest of the budget
        % to randomly sampled from each class.
        
        NotCaptured=setdiff([1:K],unique(Labels));
        
        if (length(NotCaptured)==BudgetRemaining) % If yes, get a label from each class then end
            for i=1:length(NotCaptured)
                ExtraLabel(i)=randsample(find(GT==NotCaptured(i)),1);
                Labels(ExtraLabel(i))=GT(ExtraLabel(i));
                BudgetRemaining=BudgetRemaining-1;
            end
        end
        
    end
    
end

if strcmp(SampleMethod,'Random')
    [~,IdxMax]=max(DistStruct.Density);
    Labels(IdxMax)=GT(IdxMax);
    BudgetRemaining=BudgetRemaining-1;
    while BudgetRemaining>0
        RandomIdx=randsample(size(X,1),1);
        if  GT(RandomIdx)~=0 && Labels(RandomIdx)==0
            Labels(RandomIdx)=GT(RandomIdx);
            BudgetRemaining=BudgetRemaining-1;
        end
        % Check to see if we need to stop and use the rest of the budget
        % to randomly sampled from each class.
        
        NotCaptured=setdiff([1:K],unique(Labels));
        
        if (length(NotCaptured)==BudgetRemaining) % If yes, get a label from each class then end
            for i=1:length(NotCaptured)
                ExtraLabel(i)=randsample(find(GT==NotCaptured(i)),1);
                Labels(ExtraLabel(i))=GT(ExtraLabel(i));
                BudgetRemaining=BudgetRemaining-1;
            end
        end
    end
end

%% From the active queries, label the rest of the data in order of decreasing density

% Sort points by density

[~,idx]=sort(Density,'descend');

% First pass

Idx_NN=DistStruct.Idx_NN;
D_NN=DistStruct.D_NN;

LabelsMatrix=zeros(M,N);

for j=1:length(Labels)
    if Labels(idx(j))==0
        Candidates=intersect(Idx_NN(idx(j),:),idx(1:j-1)');
        if  ~isempty(Candidates)
            Temp=find(sum(Candidates'==Idx_NN(idx(j),:),1));
            [~,NN_Idx]=min(D_NN(idx(j),Temp));
            NN=Idx_NN(idx(j),Temp(NN_Idx));
            
            
        elseif isempty(Candidates)
            D=pdist2(WeightedEigVecs,WeightedEigVecs(idx(j),:));
            [~,Temp]=min(D(idx(1:j-1)));
            NN=idx(Temp);
        end
        
        try
            Labels(idx(j))=Labels(NN);
        catch
            keyboard
        end
        
        % Special case when many value have same density
        
        if Labels(idx(j))==0
            Temp=find(Labels>0);
            D=pdist2(WeightedEigVecs,WeightedEigVecs(idx(j),:));
            [~,NN]=min(D(Temp));
            Labels(idx(j))=Labels(Temp(NN));
        end
        
        % Check spatial consensus label.
        % If there is disagreement, wait until second pass to label.
        
        LabelsMatrix(I(idx(j)),J(idx(j)))=Labels(idx(j));
        SpatialConsensusLabel=SpatialConsensus(LabelsMatrix,idx(j),M,N,I,J,WindowSize);
        
        if ~(SpatialConsensusLabel==Labels(idx(j))) && (SpatialConsensusLabel>0)
            Labels(idx(j))=0;
        end
    end
end

% Second pass

LabelsMatrix=zeros(M,N);

for j=1:length(Labels)
    if Labels(idx(j))==0
        Candidates=intersect(Idx_NN(idx(j),:),idx(1:j-1)');
        if  ~isempty(Candidates)
            Temp=find(sum(Candidates'==Idx_NN(idx(j),:),1));
            [~,NN_Idx]=min(D_NN(idx(j),Temp));
            NN=Idx_NN(idx(j),Temp(NN_Idx));
        elseif isempty(Candidates)
            D=pdist2(WeightedEigVecs,WeightedEigVecs(idx(j),:));
            [~,Temp]=min(D(idx(1:j-1)));
            NN=idx(Temp);
        end
        Labels(idx(j))=Labels(NN);
        
        % Special case when many values have same density
        
        if Labels(idx(j))==0
            Temp=find(Labels>0);
            NN=knnsearch(X(Temp,:),X(idx(j),:));
            Labels(idx(j))=Labels(Temp(NN));
        end
        
        % Enforce spatial consensus if obvious
        
        LabelsMatrix(I(idx(j)),J(idx(j)))=Labels(idx(j));
        [SpatialConsensusLabel,ConsensusFrac]=SpatialConsensus(LabelsMatrix,idx(j),M,N,I,J,WindowSize);
        if ConsensusFrac>.5
            Labels(idx(j))=SpatialConsensusLabel;
        end
    elseif Labels(idx(j))>0
        LabelsMatrix(I(idx(j)),J(idx(j)))=Labels(idx(j));
    end
end

end

