function Density = LocalDensity(X,K)

% Computes the local densitys of the points in an N x D matrix X, using the
% threshold value th.  Use K nearest neighbor.     

Density=zeros(size(X,1),1);


[~,NN_Dists]=knnsearch(X,X,'K',K); %Pairwise nearest neighbors

%For all real HSI experiments

sigma=.5*mean(NN_Dists(:));

for l=1:size(X,1)
    Density(l)=sum(exp(-NN_Dists(l,:).^2/(sigma^2)));
end

end