function [y,frac]=SpatialConsensus(Labels,Idx,M,N,I,J,WindowSize)

% For a set of labels, Labels, and a labelled point given by Idx, find the
% consensus label in a window of size WindowSize.

% Labels should be as a matrix

%Find spatial neighbors

Neighbors=FindNeighbors([I(Idx),J(Idx)],WindowSize,M,N);

%Determine labels of spatial neighbors

try

    LocalLabels=Labels(Neighbors(1,:),Neighbors(2,:));
    LocalLabels=LocalLabels(LocalLabels>0);
    
catch
    keyboard
end

if isempty(LocalLabels)
    y=0;
    frac=0;
else
    y=mode(LocalLabels);
    frac=sum(LocalLabels==y)/length(LocalLabels);
end

end