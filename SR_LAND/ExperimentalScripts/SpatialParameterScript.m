% Analyze the impact of the spatial regularity parameter in the labeling
% accuracy for active learning

clear all;
profile off
profile on;

%IP

%{
DataInput.datasource='HSI';
DataInput.HSIname='IP';
DataInput.idx1=1:145;
DataInput.idx2=1:145;
Window_Diffusion=[5:1:25];
FewQueries=50;
ManyQueries=100;
%}

%Salinas A

DataInput.datasource='HSI';
DataInput.HSIname='SalinasA';
DataInput.idx1=1:83;
DataInput.idx2=1:86;
Window_Diffusion=[5:1:20];
FewQueries=10;
ManyQueries=20;

%Pavia U

%{
DataInput.datasource='HSI';
DataInput.HSIname='PaviaU';
DataInput.idx1=1:300;
DataInput.idx2=1:100;
Window_Diffusion=[5:1:15];
FewQueries=40;
ManyQueries=80;
%}

%% Load data

[~,~,Ytuple,K_GT,d,DataParameters]=GenerateData(DataInput);
GT=DataParameters.GT;
M=DataParameters.M;
N=DataParameters.N;

%Data name
PeakOpts.Data=DataInput.HSIname;

% Run on all pixels

X=DataParameters.RawHSI;
X=reshape(X,size(X,1)*size(X,2),size(X,3));
X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1); %Normalize X

Y=reshape(DataParameters.GT,1,size(GT,1)*size(GT,2));

Yuse=UniqueGT(Y(Y>0));

PeakOpts.UserPlot=0;

%% Perform the things that only have to be done once

%Detect number of eigenvectors to use automatically
PeakOpts.DiffusionOpts.K='automatic';

%How many nearest neighbors to use for diffusion distance
PeakOpts.DiffusionOpts.kNN=100; %For comparison with SpatialDiffusion%100;

%Force probability of self-loop to exceed .5.
PeakOpts.DiffusionOpts.LazyWalk=0;

%Diffusion scaling parameter
PeakOpts.DiffusionOpts.epsilon=1;

% Set time to evaluate diffusion distance
PeakOpts.DiffusionTime=30;

%Kmeans parameters
NumReplicates=10;
MaxIterations=100;

%Set how many cores we want to learn.  If K_Learned is different from
%K_GT, it will not be possible to compare easily and immediately against
%the ground truth, though other patterns may be studied in this way.
K_Learned=K_GT;

SpatialWindowSize=3;

%How many nearest neighbors to use for KDE
DensityNN=20;

% Use angular distances?
PeakOpts.DiffusionOpts.Angular=0;

%%  Run SS active learning at different spatial windows

if isempty(GT)
    [I,J]=find(sum(DataParameters.RawHSI,3)~=0);
else
    [I,J]=find(GT>-1);
end

PeakOpts.ModeDetection='SpatialDiffusion';

for k=1:length(Window_Diffusion)
    
    PeakOpts.Window_Diffusion=Window_Diffusion(k);
    [CentersDiffusion_SS, G_SS, DistStructDiffusion_SS] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts, M, N);
    
    Labels=SR_LAND(X,K_GT,DistStructDiffusion_SS,I,J,M,N,SpatialWindowSize,FewQueries,GT,'Core',G_SS);
    [OA_FewQueries(k),AA_FewQueries(k),Kappa_FewQueries(k)]=GetAccuracies(Labels(Y>0),Yuse,K_Learned);
    
    Labels=SR_LAND(X,K_GT,DistStructDiffusion_SS,I,J,M,N,SpatialWindowSize,ManyQueries,GT,'Core',G_SS);
    [OA_ManyQueries(k),AA_ManyQueries(k),Kappa_ManyQueries(k)]=GetAccuracies(Labels(Y>0),Yuse,K_Learned);

end

%%
PeakOpts.ModeDetection='Diffusion';
[CentersDiffusion_LAND, G_LAND, DistStructDiffusion_LAND] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts, M, N);

Labels=SR_LAND(X,K_GT,DistStructDiffusion_SS,I,J,M,N,SpatialWindowSize,FewQueries,GT,'Core',G_SS);
[OA_LAND_FewQueries,~,~]=GetAccuracies(Labels(Y>0),Yuse,K_Learned);

Labels=SR_LAND(X,K_GT,DistStructDiffusion_SS,I,J,M,N,SpatialWindowSize,ManyQueries,GT,'Core',G_SS);
[OA_LAND_ManyQueries,~,~]=GetAccuracies(Labels(Y>0),Yuse,K_Learned);

%% Plot Results

h=figure;
plot(OA_FewQueries,'LineWidth',3,'Color','r');
hold on;
plot(OA_ManyQueries,'LineWidth',3,'Color','b');
title('OA as a Function of $R$','Interpreter','latex','FontSize',14);
xticks(1:5:length(Window_Diffusion));
xticklabels(Window_Diffusion(1:5:end));
xlabel('Diffusion Window','Interpreter','latex','FontSize',14);
xlim([1 15]);
xlabel('$R$','Interpreter','latex','FontSize',14);
ylabel('OA','Interpreter','latex','FontSize',14);

axis square
axis tight


legend('OA, Few Queries',...
       'OA, Many Queries',...
       'Location','southeast')