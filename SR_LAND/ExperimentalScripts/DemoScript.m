% Run SR LAND

clear all; close all;

SaveResults=0;

NumTrials=1;

%IP Data:

%{
DataInput.datasource='HSI';
DataInput.HSIname='IP';
DataInput.idx1=1:145;
DataInput.idx2=1:145;
Window_Diffusion=14;
MaxSamples=100;
SampleStep=5;
ActiveSamples=[20:SampleStep:MaxSamples];
%}

% Salinas A Data:

%{
DataInput.datasource='HSI';
DataInput.HSIname='SalinasA';
DataInput.idx1=1:83;
DataInput.idx2=1:86;
Window_Diffusion=11;
MaxSamples=20;
SampleStep=1;
ActiveSamples=[10:SampleStep:MaxSamples];
%}

%PaviaU Data:

%{
DataInput.datasource='HSI';
DataInput.HSIname='PaviaU';
DataInput.idx1=1:300;
DataInput.idx2=1:100;
Window_Diffusion=12;
MaxSamples=80;
SampleStep=2;
ActiveSamples=[40:SampleStep:MaxSamples];
%}


%% Load data

[~,~,Ytuple,K_GT,d,DataParameters]=GenerateData(DataInput);
GT=DataParameters.GT;
M=DataParameters.M;
N=DataParameters.N;

%Data name
PeakOpts.Data=DataInput.HSIname;

%Reshape and normalize data
X=DataParameters.RawHSI;
X=reshape(X,size(X,1)*size(X,2),size(X,3));
X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1);

Y=reshape(DataParameters.GT,1,size(GT,1)*size(GT,2));
Yuse=UniqueGT(Y(Y>0));

%% Perform the things that only have to be done once

%Detect number of eigenvectors to use automatically
PeakOpts.DiffusionOpts.K='automatic';

%How many nearest neighbors to use for diffusion distance
PeakOpts.DiffusionOpts.kNN=100; %For comparison with SpatialDiffusion%100;

%Diffusion scaling parameter
PeakOpts.DiffusionOpts.epsilon=1;

% Set time to evaluate diffusion distance
PeakOpts.DiffusionTime=30;

%Set how many cores we want to learn.  If K_Learned is different from
%K_GT, it will not be possible to compare easily and immediately against
%the ground truth, though other patterns may be studied in this way.
K_Learned=K_GT;

SpatialWindowSize=3;

%How many nearest neighbors to use for KDE
DensityNN=20;

% Diffusion just on spectra

PeakOpts.ModeDetection='Diffusion';

tic;
[~, G_Spectral, DistStructDiffusion_Spectral] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts, M, N);
TimeSpectralMode=toc;

% Diffusion on spatial-spectral features

PeakOpts.ModeDetection='SpatialDiffusion';
PeakOpts.Window_Diffusion=Window_Diffusion;

tic;
[~, G_SR, DistStructDiffusion_SR] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts, M, N);
TimeSpatialSpectralMode=toc;


%%  LAND variants

if isempty(GT)
    [I,J]=find(sum(DataParameters.RawHSI,3)~=0);
else
    [I,J]=find(GT>-1);
end


for l=1:length(ActiveSamples)
    
    display(['Active Parameter ',num2str(l),' out of ',num2str(length(ActiveSamples))]);
    
    % LAND with random queries
    tic;
    
    for ii=1:NumTrials
        Labels_Random(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_Spectral,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Random',G_Spectral);
        TimeRandom(l)=toc+TimeSpectralMode;
        OA_Random(l,ii)=GetAccuracies(Labels_Random(l,Y>0),Yuse,K_Learned);
    end
    
    OA_Random_Mean(l)=mean(OA_Random(l,:));
    
    % LAND
    tic;
    Labels_Core(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_Spectral,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Core',G_Spectral);
    TimeCore(l)=toc+TimeSpectralMode;
    OA_LAND(l)=GetAccuracies(Labels_Core(l,Y>0),Yuse,K_Learned);
    
    % SR LAND with random queries
    
    for ii=1:NumTrials
        tic;
        Labels_SR_Random(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_SR,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Random',G_SR);
        TimeSR_Random(l)=toc+TimeSpatialSpectralMode;
        OA_SR_Random(l,ii)=GetAccuracies(Labels_SR_Random(l,Y>0),Yuse,K_Learned);
    end
    
    OA_SR_Random_Mean(l)=mean(OA_SR_Random(l,:));
    
    
    % SR LAND
    tic;
    Labels_SR_Core(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_SR,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Core',G_SR);
    TimeSR_Core(l)=toc+TimeSpatialSpectralMode;
    OA_SR_LAND(l)=GetAccuracies(Labels_SR_Core(l,Y>0),Yuse,K_Learned);
    
end

%% Plot data

figure;
imagesc(sum(DataParameters.RawHSI,3));
axis square;
axis off;

GT_New=UniqueGT(GT);
Y_New=GT_New(:);

figure;
imagesc(GT_New);
colorbar
axis square;
axis off;

Candidates=find(Y>0);
RandomIdxs=randsample(Candidates,100);

ColorMap=colormap(parula);

figure;
for k=1:length(RandomIdxs)
    plot(X(RandomIdxs(k),:),'Color',ColorMap(round((256/K_GT)*Y_New(RandomIdxs(k))),:));
    hold on;
    xlim([0 length(X(1,:))]);
end
axis square


%% Plot of accuracy across all methods and all sampling levels

figure;
r{1}=plot(ActiveSamples,OA_LAND,'r','LineWidth',3);
hold on;
r{2}=plot(ActiveSamples,OA_SR_LAND,'k','LineWidth',3);

legend({'LAND',...
    'SR LAND'},...
    'Location', 'southeast', 'FontSize', 6);

xlim([min(ActiveSamples) max(ActiveSamples)]);

xlabel('Number of Training Samples','Interpreter','Latex','FontSize',16);
ylabel('Overall Accuracy','Interpreter','Latex','FontSize',16);
title('Performance of Active Learning Algorithms','Interpreter','Latex','FontSize',16);
axis square;


%% Organize accuracies at particular sampling levels into vectors

if strcmp(DataInput.HSIname,'IP')
    
    SmallSampleIdx=7;
    LargeSampleIdx=17;
    
elseif strcmp(DataInput.HSIname,'SalinasA')
    
    SmallSampleIdx=1;
    LargeSampleIdx=11;
    
elseif strcmp(DataInput.HSIname,'PaviaU')
    
    SmallSampleIdx=4;
    LargeSampleIdx=10;
    
end


OA_SmallSamples(1)=OA_LAND(SmallSampleIdx);
OA_SmallSamples(2)=OA_SR_LAND(SmallSampleIdx);

OA_LargeSamples(1)=OA_LAND(LargeSampleIdx);
OA_LargeSamples(2)=OA_SR_LAND(LargeSampleIdx);

%% Plot images of results at various sampling levels

figure;
imagesc(reshape(Labels_SR_Core(SmallSampleIdx,:),M,N).*(GT>0));
axis square;
axis off;

figure;
imagesc(reshape(Labels_SR_Core(LargeSampleIdx,:),M,N).*(GT>0));
axis square;
axis off;

%% Display runtimes

display(['Time LAND = ',num2str(mean(TimeCore(:))) newline]);
display(['Time SR LAND = ',num2str(mean(TimeSR_Core(:))) newline]);

%% Save Results

if SaveResults
    save(['Results_',DataInput.HSIname,datetime]);
end

