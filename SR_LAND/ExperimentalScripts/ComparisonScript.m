% Compare the SR LAND active learning method for HSI to a range of
% comparison methods.

clear all; close all;

SaveResults=0;

NumTrials=10;

%IP Data:

%{
DataInput.datasource='HSI';
DataInput.HSIname='IP';
DataInput.idx1=1:145;
DataInput.idx2=1:145;
Window_Diffusion=14;
MaxSamples=100;
SampleStep=5;
ActiveSamples=[20:SampleStep:MaxSamples];
%}

% Salinas A Data:

%{
DataInput.datasource='HSI';
DataInput.HSIname='SalinasA';
DataInput.idx1=1:83;
DataInput.idx2=1:86;
Window_Diffusion=11;
MaxSamples=20;
SampleStep=1;
ActiveSamples=[10:SampleStep:MaxSamples];
%}

%PaviaU Data:

DataInput.datasource='HSI';
DataInput.HSIname='PaviaU';
DataInput.idx1=1:300;
DataInput.idx2=1:100;
Window_Diffusion=12;
MaxSamples=80;
SampleStep=2;
ActiveSamples=[40:SampleStep:MaxSamples];


%% Load data

[~,~,Ytuple,K_GT,d,DataParameters]=GenerateData(DataInput);
GT=DataParameters.GT;
M=DataParameters.M;
N=DataParameters.N;

%Data name
PeakOpts.Data=DataInput.HSIname;

%Reshape and normalize data
X=DataParameters.RawHSI;
X=reshape(X,size(X,1)*size(X,2),size(X,3));
X=X./repmat(sqrt(sum(X.*X,1)),size(X,1),1);

Y=reshape(DataParameters.GT,1,size(GT,1)*size(GT,2));
Yuse=UniqueGT(Y(Y>0));

%% Perform the things that only have to be done once

%Detect number of eigenvectors to use automatically
PeakOpts.DiffusionOpts.K='automatic';

%How many nearest neighbors to use for diffusion distance
PeakOpts.DiffusionOpts.kNN=100; %For comparison with SpatialDiffusion%100;

%Diffusion scaling parameter
PeakOpts.DiffusionOpts.epsilon=1;

% Set time to evaluate diffusion distance
PeakOpts.DiffusionTime=30;

%Set how many cores we want to learn.  If K_Learned is different from
%K_GT, it will not be possible to compare easily and immediately against
%the ground truth, though other patterns may be studied in this way.
K_Learned=K_GT;

SpatialWindowSize=3;

%How many nearest neighbors to use for KDE
DensityNN=20;

% Diffusion just on spectra

PeakOpts.ModeDetection='Diffusion';

tic;
[~, G_Spectral, DistStructDiffusion_Spectral] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts, M, N);
TimeSpectralMode=toc;

% Diffusion on spatial-spectral features

PeakOpts.ModeDetection='SpatialDiffusion';
PeakOpts.Window_Diffusion=Window_Diffusion;

tic;
[~, G_SR, DistStructDiffusion_SR] = DensityPeaksEstimation(X, K_GT, DensityNN, PeakOpts, M, N);
TimeSpatialSpectralMode=toc;

%%  LAND variants

if isempty(GT)
    [I,J]=find(sum(DataParameters.RawHSI,3)~=0);
else
    [I,J]=find(GT>-1);
end


for l=1:length(ActiveSamples)
    
    display(['Active Parameter ',num2str(l),' out of ',num2str(length(ActiveSamples))]);
    
    % LAND with random queries
    tic;
    
    for ii=1:NumTrials
        Labels_Random(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_Spectral,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Random',G_Spectral);
        TimeRandom(l)=toc+TimeSpectralMode;
        OA_Random(l,ii)=GetAccuracies(Labels_Random(l,Y>0),Yuse,K_Learned);
    end
    
    OA_Random_Mean(l)=mean(OA_Random(l,:));
    
    % LAND
    tic;
    Labels_LAND(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_Spectral,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Core',G_Spectral);
    TimeLAND(l)=toc+TimeSpectralMode;
    OA_LAND(l)=GetAccuracies(Labels_LAND(l,Y>0),Yuse,K_Learned);
    
    % SR LAND with random queries
    
    for ii=1:NumTrials
        tic;
        Labels_SR_Random(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_SR,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Random',G_SR);
        TimeSR_Random(l)=toc+TimeSpatialSpectralMode;
        OA_SR_Random(l,ii)=GetAccuracies(Labels_SR_Random(l,Y>0),Yuse,K_Learned);
    end
    
    OA_SR_Random_Mean(l)=mean(OA_SR_Random(l,:));
    
    
    % SR LAND
    tic;
    Labels_SR_LAND(l,:)=SR_LAND(X,K_GT,DistStructDiffusion_SR,I,J,M,N,SpatialWindowSize,ActiveSamples(l),GT,'Core',G_SR);
    Time_SR_LAND(l)=toc+TimeSpatialSpectralMode;
    OA_SR_LAND(l)=GetAccuracies(Labels_SR_LAND(l,Y>0),Yuse,K_Learned);
    
end

%%  Comparison methods:
%   Random SVM
%   LAND Queries SVM
%   Margin SVM
%   Multiview SVM
%   EQB SVM
%   IFRF SVM
%   LBP

Idx_Labels=find(Y>0);
[~,Queries_LAND]=sort(DistStructDiffusion_SR.Delta,'descend');
Queries_LAND=intersect(Queries_LAND,Idx_Labels,'stable');
for ii=1:NumTrials
    
    % Random Queries
    
    Queries_Random=randsample(Idx_Labels,ActiveSamples(end));
    
    % SVM with Random Queries
    
    GroundT(1,:)=1:length(Y);
    GroundT(2,:)=Y';
    
    for l=1:length(ActiveSamples)
        
        train_SL = GroundT(:,Queries_Random(1:ActiveSamples(l)));
        train_samples = X(train_SL(1,:),:);
        train_labels= train_SL(2,:)';
        
        test_SL = GroundT;
        test_samples = X(test_SL(1,:),:);
        test_labels = test_SL(2,:)';
        
        % Selecting the paramter for SVM
        tic;
        [Ccv, Gcv, cv, cv_t]=cross_validation_svm(train_labels,train_samples);
        % Training using a Gaussian RBF kernel
        parameter=sprintf('-c %f -g %f -m 500 -t 2 -q',Ccv,Gcv);
        model=svmtrain(train_labels,train_samples,parameter);
        % Testing
        Labels_SVM_Random(l,:) = svmpredict(ones(M*N,1),X,model);
        Time_SVM_Random(l,ii)=toc;
        OA_SVM_Random(l,ii)=GetAccuracies(Labels_SVM_Random(l,Y>0),Yuse,K_Learned);
        
    end
    
    % SVM with LAND Queries
    
    for l=1:length(ActiveSamples)
        
        train_SL = GroundT(:,Queries_LAND(1:ActiveSamples(l)));
        train_samples = X(train_SL(1,:),:);
        train_labels= train_SL(2,:)';
        
        test_SL = GroundT;
        test_samples = X(test_SL(1,:),:);
        test_labels = test_SL(2,:)';
        
        % Selecting the paramter for SVM
        tic;
        [Ccv, Gcv, cv, cv_t]=cross_validation_svm(train_labels,train_samples);
        % Training using a Gaussian RBF kernel
        parameter=sprintf('-c %f -g %f -m 500 -t 2 -q',Ccv,Gcv);
        model=svmtrain(train_labels,train_samples,parameter);
        % Testing
        Labels_SVM_LAND(l,:) = svmpredict(ones(M*N,1),X,model);
        Time_SVM_LAND(l,ii)=toc;
        OA_SVM_LAND(l,ii)=GetAccuracies(Labels_SVM_LAND(l,Y>0),Yuse,K_Learned);
        
    end
    
    % Image Fusion and Recursive Filtering (IFRF)
    
    tic;
    img2=average_fusion(reshape(X,M,N,d),size(X,2));
    
    fimg=reshape(img2,[M*N, size(img2,3)]);
    [fimg] = scale_new(fimg);
    fimg=reshape(fimg,[M,N,size(fimg,2)]);
    % IFRF feature construction
    fimg=spatial_feature(fimg,200,0.3);
    
    % SVM classification
    fimg = ToVector(fimg);
    fimg = fimg;
    fimg=double(fimg);
    TimeIFRF=toc;
    
    for l=1:length(ActiveSamples)
        
        train_SL = GroundT(:,Queries_Random(1:ActiveSamples(l)));
        train_samples = fimg(train_SL(1,:),:);
        train_labels= train_SL(2,:)';
        
        test_SL = GroundT;
        test_samples = fimg(test_SL(1,:),:);
        test_labels = test_SL(2,:)';
        
        % Selecting the paramter for SVM
        tic;
        [Ccv, Gcv, cv, cv_t]=cross_validation_svm(train_labels,train_samples);
        % Training using a Gaussian RBF kernel
        parameter=sprintf('-c %f -g %f -m 500 -t 2 -q',Ccv,Gcv);
        model=svmtrain(train_labels,train_samples,parameter);
        % Testing
        Time_SVM_IFRF(l,ii)=toc+TimeIFRF;
        Labels_SVM_IFRF(l,:) = svmpredict(ones(M*N,1),fimg,model);
        OA_SVM_IFRF(l,ii)=GetAccuracies(Labels_SVM_IFRF(l,Y>0),Yuse,K_Learned);
        
    end
    
    % Tuia Toolbox Methods
    
    num_of_classes = K_GT;
    Y_Rescaled = Y-1; % classes must start at 0 for SVMtorch
    
    IdxLabeled=find(Y>0);
    
    s = rand('twister');
    rand('twister',0);
    c = randperm(length(IdxLabeled))';
    rand('twister',s);
    
    %Make sure there is at least one representative of each class
    
    
    
    tr = [X(IdxLabeled(c(1:2*K_GT)),:) Y_Rescaled(IdxLabeled(c(1:2*K_GT)))'];
    cand = [X(IdxLabeled(c(2*K_GT+1:end)),:) Y_Rescaled(IdxLabeled(c(2*K_GT+1:end)))'];
    ts = [X(IdxLabeled(c(1:end)),:) Y_Rescaled(IdxLabeled(c(1:end)))'];
    
    NotCaptured=setdiff(unique(Y_Rescaled(Y>0)),unique(tr(:,end)));
    
    for i=1:length(NotCaptured)
        ExtraLabel(i)=randsample(find(Y_Rescaled==NotCaptured(i)),1);
        tr(end+1,:)=[X(ExtraLabel(i),:) Y_Rescaled(ExtraLabel(i))];
    end
    
    
    iterVect = ActiveSamples;
    
    options.model = 'SVM';
    options.uncertainty = 'MS';
    options.diversity = 'None';
    options.iterVect = iterVect;
    
    tic;
    
    [OA_SVM_MarginSampling_Temp, ~, ~, ~, ~] = ...
        AL(tr, cand, ts, num_of_classes, options);
    
    Time_SVM_MarginSampling(l,ii)=toc;
    
    OA_SVM_MarginSampling(:,ii)=OA_SVM_MarginSampling_Temp(:,2);
    
    options.model = 'SVM';
    options.uncertainty = 'Multiview';
    options.diversity = 'None';
    options.iterVect = iterVect;
    
    tic;
    
    [OA_SVM_Multiview_Temp, ~, ~, ~, ~] = ...
        AL(tr, cand, ts, num_of_classes, options);
    
    Time_SVM_Multiview(l,ii)=toc;
    
    
    OA_SVM_Multiview(:,ii)=OA_SVM_Multiview_Temp(:,2);
    
    options.model = 'SVM';
    options.uncertainty = 'EQB';
    options.diversity = 'None';
    options.iterVect = iterVect;
    
    tic;
    
    [OA_SVM_EQB_Temp, ~, ~, ~, ~] = ...
        AL(tr, cand, ts, num_of_classes, options);
    
    Time_SVM_EQB(l,ii)=toc;
    OA_SVM_EQB(:,ii)=OA_SVM_EQB_Temp(:,2);
    
    tic;
    [OA_LBP_Temp,t]=LBP(X,Y,M,N,d,K_GT,DataInput,MaxSamples,SampleStep);
    Time_SVM_LBP(l,ii)=toc;
    
    OA_LBP(:,ii)=OA_LBP_Temp;
    
end

%%  Compute means

OA_SVM_Random_Mean=mean(OA_SVM_Random,2);
OA_SVM_IFRF_Mean=mean(OA_SVM_IFRF,2);
OA_SVM_MarginSampling_Mean=mean(OA_SVM_MarginSampling,2);
OA_SVM_Multiview_Mean=mean(OA_SVM_Multiview,2);
OA_SVM_EQB_Mean=mean(OA_SVM_EQB,2);
OA_LBP_Mean=mean(OA_LBP,2);
OA_SVM_LAND=mean(OA_SVM_LAND,2);

%% Plot data

figure;
imagesc(sum(DataParameters.RawHSI,3));
axis square;
axis off;

GT_New=UniqueGT(GT);
Y_New=GT_New(:);

figure;
imagesc(GT_New);
colorbar
axis square;
axis off;

Candidates=find(Y>0);
RandomIdxs=randsample(Candidates,100);

ColorMap=colormap(parula);

figure;
for k=1:length(RandomIdxs)
    plot(X(RandomIdxs(k),:),'Color',ColorMap(round((256/K_GT)*Y_New(RandomIdxs(k))),:));
    hold on;
    xlim([0 length(X(1,:))]);
end
axis square


%% Plot of accuracy across all methods and all sampling levels

figure;
r{1}=plot(ActiveSamples,OA_SVM_Random_Mean,'y--','LineWidth',3);
hold on;
r{2}=plot(ActiveSamples,OA_SVM_MarginSampling_Mean,'c--','LineWidth',3);
hold on;
r{3}=plot(ActiveSamples,OA_SVM_Multiview_Mean,'r--','LineWidth',3);
hold on;
r{4}=plot(ActiveSamples,OA_SVM_EQB_Mean,'g--','LineWidth',3);
hold on;
r{6}=plot(t,OA_LBP_Mean,'k--','LineWidth',3);
hold on;
r{5}=plot(ActiveSamples,OA_SVM_IFRF_Mean,'b--','LineWidth',3);
hold on;
r{7}=plot(ActiveSamples,OA_SVM_LAND,'b','LineWidth',3);
hold on;
r{8}=plot(ActiveSamples,OA_LAND,'r','LineWidth',3);
hold on;
r{9}=plot(ActiveSamples,OA_SR_LAND,'k','LineWidth',3);

legend({'Random SVM',...
    'MS SVM',...
    'MV SVM',...
    'EQB SVM',...
    'LBP',...
    'IFRF SVM',....
    'LAND SVM',...
    'LAND',...
    'SR LAND'},...
    'Location', 'southeast', 'FontSize', 6);

xlim([min(ActiveSamples) max(ActiveSamples)]);

xlabel('Number of Training Samples','Interpreter','Latex','FontSize',16);
ylabel('Overall Accuracy','Interpreter','Latex','FontSize',16);
title('Performance of Active Learning Algorithms','Interpreter','Latex','FontSize',16);
axis square;


%% Organize accuracies at particular sampling levels into vectors

if strcmp(DataInput.HSIname,'IP')
    
    SmallSampleIdx=7;
    LargeSampleIdx=17;
    
elseif strcmp(DataInput.HSIname,'SalinasA')
    
    SmallSampleIdx=1;
    LargeSampleIdx=11;
    
elseif strcmp(DataInput.HSIname,'PaviaU')
    
    SmallSampleIdx=4;
    LargeSampleIdx=10;
    
end

OA_SmallSamples(1)=OA_SVM_Random_Mean(SmallSampleIdx);
OA_SmallSamples(2)=OA_SVM_MarginSampling_Mean(SmallSampleIdx);
OA_SmallSamples(3)=OA_SVM_Multiview_Mean(SmallSampleIdx);
OA_SmallSamples(4)=OA_SVM_EQB_Mean(SmallSampleIdx);
OA_SmallSamples(5)=OA_LBP_Mean(length(t)-length(ActiveSamples)+SmallSampleIdx);
OA_SmallSamples(6)=OA_SVM_IFRF_Mean(SmallSampleIdx);
OA_SmallSamples(7)=OA_SVM_LAND(SmallSampleIdx);
OA_SmallSamples(8)=OA_LAND(SmallSampleIdx);
OA_SmallSamples(9)=OA_SR_LAND(SmallSampleIdx);


OA_LargeSamples(1)=OA_SVM_Random_Mean(LargeSampleIdx);
OA_LargeSamples(2)=OA_SVM_MarginSampling_Mean(LargeSampleIdx);
OA_LargeSamples(3)=OA_SVM_Multiview_Mean(LargeSampleIdx);
OA_LargeSamples(4)=OA_SVM_EQB_Mean(LargeSampleIdx);
OA_LargeSamples(5)=OA_LBP_Mean(length(t)-length(ActiveSamples)+LargeSampleIdx);
OA_LargeSamples(6)=OA_SVM_IFRF_Mean(LargeSampleIdx);
OA_LargeSamples(7)=OA_SVM_LAND(LargeSampleIdx);
OA_LargeSamples(8)=OA_LAND(LargeSampleIdx);
OA_LargeSamples(9)=OA_SR_LAND(LargeSampleIdx);

%% Plot images of results at various sampling levels

figure;
imagesc(reshape(Labels_SR_LAND(SmallSampleIdx,:),M,N).*(GT>0));
axis square;
axis off;

figure;
imagesc(reshape(Labels_SR_LAND(LargeSampleIdx,:),M,N).*(GT>0));
axis square;
axis off;

%% Display runtimes

display(['Time Random SVM= ',num2str(mean(Time_SVM_Random(:))) newline]);
display(['Time MS SVM = ',num2str(mean(Time_SVM_MarginSampling(:))) newline]);
display(['Time MV SVM = ',num2str(mean(Time_SVM_Multiview(:))) newline]);
display(['Time EQB SVM = ',num2str(mean(Time_SVM_EQB(:))) newline]);
display(['Time LBP = ',num2str(mean(Time_SVM_LBP(:))) newline]);
display(['Time IFRF SVM = ',num2str(mean(Time_SVM_IFRF(:))) newline]);
display(['Time LAND SVM= ',num2str(mean(Time_SVM_LAND(:))) newline]);
display(['Time LAND = ',num2str(mean(TimeLAND(:))) newline]);
display(['Time SR LAND = ',num2str(mean(Time_SR_LAND(:))) newline]);

%% Save Results

if SaveResults
    save(['Results_',DataInput.HSIname,datetime]);
end

